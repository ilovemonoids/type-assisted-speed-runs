# Endpoints

This document serves as an overview of the endpoints this application
supports.

## Synopsis

```
-- core
POST /register?email=...&password=...
POST /login
  Authorization: Basic:base64(user:password)

-- players
GET /players
GET /players/:id
DELETE /players/:id

-- profile management
GET /profiles/:id
PUT /profiles/:id  -- bio, display name, password
POST /profiles/:id/image

-- images
GET /images/:id
DELETE /images/:id

-- games
POST /games        -- admin
GET /games
GET /games/:id
PUT /games/:id     -- admin
DELETE /games/:id  -- admin

-- runs
GET /runs
GET /runs/:id
POST /runs/:id/archive
GET /games/:id/runs

-- player runs
GET /players/:id/runs

-- game runs
GET /games/:id/runs
POST /games/:id/runs
```
