CREATE TABLE IF NOT EXISTS tas.image (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  iname varchar(64) NOT NULL,
  content bytea NOT NULL
);

CREATE TABLE IF NOT EXISTS tas.player (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  display_name varchar(64) NOT NULL,
  email varchar(254) NOT NULL,
  password_hash bytea NOT NULL,
  joined timestamptz NOT NULL,
  last_login timestamptz NOT NULL,
  UNIQUE (email)
);

CREATE TABLE IF NOT EXISTS tas.profile (
  player_id uuid PRIMARY KEY NOT NULL REFERENCES tas.player (id),
  image_id uuid NOT NULL REFERENCES tas.image (id),
  bio varchar(2048) NOT NULL,
  UNIQUE (player_id)
);

CREATE TABLE IF NOT EXISTS tas.run (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  link varchar(2000) NOT NULL,
  duration serial4 NOT NULL,
  description varchar(2048) NOT NULL,
  uploaded timestamptz NOT NULL,
  player_id uuid NOT NULL REFERENCES tas.player (id)
);

CREATE TABLE IF NOT EXISTS tas.run_archive (
  run_id uuid NOT NULL REFERENCES tas.run (id),
  player_id uuid NOT NULL REFERENCES tas.player (id)
);

CREATE TABLE IF NOT EXISTS tas.game (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  gname varchar(256) NOT NULL,
  published timestamptz,
  publisher varchar(256),
  image_id uuid NOT NULL REFERENCES tas.image (id)
);

CREATE TABLE IF NOT EXISTS tas.player_runs (
  player_id uuid NOT NULL REFERENCES tas.player (id),
  run_id uuid NOT NULL REFERENCES tas.run (id)
);

CREATE TABLE IF NOT EXISTS tas.game_runs (
  game_id uuid NOT NULL REFERENCES tas.game (id),
  run_id uuid NOT NULL REFERENCES tas.run (id)
);

CREATE TABLE IF NOT EXISTS tas.player_games (
  player_id uuid NOT NULL REFERENCES tas.player (id),
  game_id uuid NOT NULL REFERENCES tas.game (id)
);
