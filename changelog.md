# 0.3.1.0 (Sep. 24, 2015)

* Implement viewPlayer
* Add support for NotFound errors
* Add support for generic bad request errors

# 0.3.0.0 (Sep. 23, 2015)

* Add logging support

# 0.2.0.0 (Sep. 23, 2015)

* Add support for user registration
* Add support for logins and sessions
* Update DB model:
  * no longer have separate user_name field
  * change type of password_hash from varbit -> bytea
* Create initial route skeleton
* Add support for informative error handling
* Create User JSON response view

# 0.1.0.0 (Sep. 20, 2015)

* Initial commit
