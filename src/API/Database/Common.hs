{-# LANGUAGE FlexibleContexts, ConstraintKinds, RankNTypes #-}
module API.Database.Common (
  Cx,
  Tx,
  Val,
  Row,
  Pool,
  SQLError,
  One,

  one,
  serialW,
  serialR
) where

import Data.Functor.Identity
import qualified Hasql as H
import qualified Hasql.Backend as HB
import qualified Hasql.Postgres as HP

type Cx = HB.CxTx HP.Postgres
type Tx a = forall s. H.Tx HP.Postgres s a
type Val t = HB.CxValue HP.Postgres t
type Row t = H.CxRow HP.Postgres t
type Pool = H.Pool HP.Postgres
type SQLError = H.SessionError HP.Postgres
type One = Identity

serialW :: Maybe (HB.TxIsolationLevel, Maybe Bool)
serialW = Just (H.Serializable, Just True)

serialR :: Maybe (HB.TxIsolationLevel, Maybe Bool)
serialR = Just (H.Serializable, Nothing)

one :: Identity a -> a
one (Identity a) = a
