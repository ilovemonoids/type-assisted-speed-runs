module API.Controllers.Players (
  viewPlayer
) where

import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import Data.UUID (fromText)
import Web.Spock.Shared

import API.Controllers.Common
import API.Responses.User
import API.Database.Common
import API.Database.User
import API.Models.Common
import API.Errors

viewPlayer :: Pool -> Text -> ActionT IO ()
viewPlayer sqlC playerId =
  case fromText playerId of
    Nothing    -> errorResponse (BadRequest "Invalid player ID: expecting UUID")
    (Just pid) -> do
       player <- liftIO $ lookupPlayerById sqlC (PlayerId pid)
       case player of
         Left e -> errorResponse e
         Right p -> json (ViewUser p)
