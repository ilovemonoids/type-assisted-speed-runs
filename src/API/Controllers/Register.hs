module API.Controllers.Register (
  registerUser
) where

import Control.Monad.IO.Class
import Web.Spock.Shared

import API.Models.Common
import API.Controllers.Common
import API.Database.Common
import API.Logging
import API.Responses.User
import qualified API.Database.User as DB

registerUser :: MonadIO m => Pool -> Log -> ActionT m b
registerUser sqlC logger = do
  email <- param' "email"
  pass <- param' "password"
  ret <- liftIO $ DB.register sqlC (Email email) (Pass pass)
  case ret of
    Left e  -> errorResponse e
    Right u -> do
      liftIO $ info logger Registration Post $ "Registered " <> email
      created (ViewUser u)
