{-# LANGUAGE FlexibleContexts #-}
module API.Controllers.Login (
  login,
  endSession
  -- logout
) where

import Control.Monad (void)
import Control.Monad.IO.Class
import Data.Text
import Data.Text.Encoding (encodeUtf8)
import Data.UUID (toASCIIBytes, UUID)
import Data.UUID.V4 (nextRandom)
import Database.Redis (runRedis)
import Web.Spock
import qualified Data.ByteString.Base64 as B64
import qualified Data.Text.Encoding as T
import qualified Database.Redis as R

import API.Controllers.Common
import API.Database.Common
import API.Errors
import API.Logging
import API.Models.Common
import qualified API.Database.User as DB

data SessionCheck
  = ValidSession
  | InvalidSession

data AuthCheck
  = AuthOkay Email Pass
  | AuthInvalid
  | AuthError
  | AuthMissing

login :: Pool -> R.Connection -> Log -> ActionT IO ()
login sqlC redisC logger = do
  auth <- header "Authorization"
  ret <- checkSession redisC
  case ret of
    ValidSession -> noContent
    InvalidSession -> do
      auth' <- liftIO $ checkAuth sqlC auth
      case auth' of
        AuthError -> errorResponse AuthenticationDown
        (AuthOkay (Email n) _) -> do
          skey <- liftIO $ makeSession redisC logger
          setSession skey
          liftIO $ info logger Login Post $ n <> " has logged in"
          -- TODO: update last_login field for users
          noContent
        AuthMissing -> errorResponse Unauthorized
        AuthInvalid -> errorResponse BadCredentials

checkAuth :: Pool -> Maybe Text -> IO AuthCheck
checkAuth sqlC (Just k) =
  case parseBasicAuth k of
    AuthOkay n p -> do
      ret <- DB.authenticate sqlC n p
      return $ case ret of
        Right DB.AuthSuccess -> AuthOkay n p
        Right DB.AuthFailed -> AuthInvalid
        Left _ -> AuthError
    e -> return e
checkAuth _ Nothing = return AuthMissing

checkSession :: R.Connection -> ActionT IO SessionCheck
checkSession redisC = do
  key <- getSession
  case key of
    Nothing -> return InvalidSession
    (Just k) -> do
       ret <- liftIO $ runRedis redisC (R.exists (encodeUtf8 k))
       return $ case ret of
         (Right True) -> ValidSession
         _            -> InvalidSession

endSession :: R.Connection -> Text -> IO ()
endSession redisC key =
  let k = [encodeUtf8 key]
  in void (runRedis redisC (R.del k))

makeSession :: R.Connection -> Log -> IO UUID
makeSession redisC logger = do
  u <- nextRandom
  let key = toASCIIBytes u
  reply <- runRedis redisC (R.set key "" >> R.expire key 1200)
  case reply of
    (Left x) -> err logger Redis Post $ show x
    (Right _) -> return ()
  return u

parseBasicAuth :: Text -> AuthCheck
parseBasicAuth authH =
  case splitOn "Basic: " authH of
    ["", k] -> parseAuth k
    _ -> AuthInvalid
  where parseAuth k =
          let k' = B64.decode (T.encodeUtf8 k)
          in case fmap (splitOn ":" . T.decodeUtf8) k' of
            Left _ -> AuthInvalid
            Right ["",_] -> AuthInvalid
            Right [_,""] -> AuthInvalid
            Right [n,p] -> AuthOkay (Email n) (Pass p)
            Right _     -> AuthInvalid
