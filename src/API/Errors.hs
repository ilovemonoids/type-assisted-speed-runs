{-# LANGUAGE OverloadedStrings #-}
module API.Errors (
  APIError(..),
  ViewErrors(..)
) where

import Data.Aeson
import Data.Text
import Data.UUID

data APIError
  = Unknown Text
  | AuthenticationDown
  | Unauthorized
  | BadCredentials
  | UserExists
  | DBError Text
  | BadRequest Text
  | NotFound UUID

newtype ViewErrors = ViewErrors [APIError]

instance ToJSON APIError where
  toJSON (Unknown t) =
    object [ "type" .= ("unknown" :: Text)
           , "description" .= t
           ]
  toJSON AuthenticationDown =
    object [ "type" .= ("authentication_down" :: Text) ]
  toJSON Unauthorized =
    object [ "type" .= ("unauthorized" :: Text) ]
  toJSON BadCredentials =
    object [ "type" .= ("incorrect_credentials" :: Text) ]
  toJSON (DBError t) =
    object [ "type" .= ("database_error" :: Text)
           , "description" .= t
           ]
  toJSON UserExists =
    object [ "type" .= ("user_exists" :: Text)
           , "description" .= ("This email is already registered." :: Text)
           ]
  toJSON (BadRequest desc) =
    object [ "type" .= ("bad_request" :: Text)
           , "description" .= desc
           ]
  toJSON (NotFound id') =
    object [ "type" .= ("not_found" :: Text)
           , "id" .= toText id'
           , "description" .= ("Entity not found" :: Text)
           ]

instance ToJSON ViewErrors where
  toJSON (ViewErrors es) =
    object [ "errors" .= toJSON es ]
