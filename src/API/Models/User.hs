module API.Models.User (
  User(..)
) where

import API.Models.Common

data User
  = User { pid :: PlayerId
         , name :: DisplayName
         , email :: Email
         , phash :: PassHash
         , joined :: Joined
         , lastLogin :: LastLogin
         }
